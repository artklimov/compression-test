﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arkl.Veeam.CompressionTest.App
{
    public static class AppArgsOutputWriter
    {
        public static void WriteError(AppArgsReader argsReader)
        {
            Console.WriteLine("Bad input:");
            foreach (var error in argsReader.Errors)
            {
                Console.WriteLine(error);
            }
            Console.WriteLine("\nUsage:\n{0}", AppArgsReader.GetUsageHint());
        }
    }
}
