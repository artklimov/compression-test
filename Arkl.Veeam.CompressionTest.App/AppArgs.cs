﻿namespace Arkl.Veeam.CompressionTest.App
{
    public class AppArgs
    {
        public bool Compress { get; private set; }
        public bool Decompress { get; private set; }
        public string InputFile { get; private set; }
        public string OutputFile { get; private set; }
    }
}
