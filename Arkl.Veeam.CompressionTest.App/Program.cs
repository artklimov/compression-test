﻿using Arkl.Veeam.CompressionTest.App.Gzip;
using System;
using System.IO;

namespace Arkl.Veeam.CompressionTest.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var argsReader = new AppArgsReader(args);
            if (!argsReader.IsValid())
            {
                AppArgsOutputWriter.WriteError(argsReader);
                Environment.Exit(1);
            }
            var argsObj = argsReader.GetAppArgs();
            var gzip = new SimpleGzip();
            if (argsObj.Compress)
            {
                try
                {
                    gzip.Compress(OpenInput(argsObj.InputFile), OpenOutput(argsObj.OutputFile));
                }
                catch(Exception e)
                {
                    Console.WriteLine("Error occured: {0}", e.Message);
                    Environment.Exit(1);
                }
            }
        }

        private static Stream OpenInput(string path)
        {
            throw new NotImplementedException();
        }

        private static Stream OpenOutput(string path)
        {
            throw new NotImplementedException();
        }
    }
}
