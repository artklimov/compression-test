﻿using System.IO;

namespace Arkl.Veeam.CompressionTest.App.Gzip
{
    public interface IGzip
    {
        void Compress(Stream input, Stream output);
        void Decompress(Stream input, Stream output);
    }
}
