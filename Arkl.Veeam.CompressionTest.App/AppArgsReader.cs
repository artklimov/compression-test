﻿using System;

namespace Arkl.Veeam.CompressionTest.App
{
    public class AppArgsReader
    {
        private const string UsageHint = "[complress/decompress] [input file] [output file]";
        
        private string[] _args;
        private string[] _errors;

        public AppArgsReader(string[] args)
        {
            this._args = args;
        }

        public bool IsValid()
        {
            throw new NotImplementedException();
        }

        public string[] Errors
        {
            get { return this._errors; }
        }

        public static string GetUsageHint()
        {
            return UsageHint;
        }

        public AppArgs GetAppArgs()
        {
            return new AppArgs();
        }
    }
}
